const scrollEventsTo = (x = 34) => {
  const eventsBox = document.querySelector("#eventsBox");
  const REM = Number(
    getComputedStyle(document.getElementsByTagName("html")[0])
      .fontSize.split("")
      .slice(0, -2)
      .join("")
  );
  eventsBox.scrollTo(x * REM, 0);
};

const termSwitch1 = document.querySelector("#term-switch-1");
const termSwitch2 = document.querySelector("#term-switch-2");
const termSwitchIndicator = document.querySelector("#term-switch_indicator");

const term1 = document.querySelector("#term1");
const term2 = document.querySelector("#term2");

termSwitch2.classList.toggle("active");
activeTermSwitch = termSwitch2;
term2.classList.toggle("active");

activeTerm = activeTermSwitch.dataset.term;
console.log(activeTerm);
[termSwitch1, termSwitch2].forEach((termSwitch) => {
  termSwitch.addEventListener("click", (e) => {
    if (activeTermSwitch != termSwitch) {
      // console.log("Switch");
      activeTermSwitch.classList.toggle("active");
      activeTermSwitch = termSwitch;
      activeTermSwitch.classList.toggle("active");

      activeTerm = activeTermSwitch.dataset.term;

      term2.classList.toggle("active");
      term1.classList.toggle("active");

      console.log(activeTerm);

      termSwitchIndicator.classList.toggle("base");
    }
  });
});
